﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innergy
{
    public class Material
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public int Amount { get; set; }
        public Material()
        {

        }
    }
}
