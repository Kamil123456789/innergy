﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innergy
{
    public class Inventory
    {
        public string Name { get; set; }
        public List<Material> Materials { get; set; }

        public int TotalAmount { 
            get
            {
                int totalAmount = 0;

                foreach(Material material in Materials)
                {
                    totalAmount += material.Amount;
                }

                return totalAmount;
            } 
        } 

        public Inventory()
        {

        }
    }
}
