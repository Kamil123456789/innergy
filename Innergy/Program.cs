﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Innergy
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Inventory> inventories = new List<Inventory>();
            List<Inventory> inventoriesTomerge = new List<Inventory>();

            string record;

            while (!string.IsNullOrWhiteSpace(record = Console.ReadLine()))
            {
                inventoriesTomerge = InventoryParser.ParseRecord(record);
                inventories = InventoryParser.MergeInventories(inventories, inventoriesTomerge);
            }

            inventories = InventoryParser.SortInventories(inventories);
            InventoryParser.PrintInventories(inventories);

        }
    }
}
