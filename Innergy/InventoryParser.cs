﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innergy
{
    public static class InventoryParser
    {
        public static Dictionary<string, int> ParseInventories(string inventories)
        {
            string[] splittedInventories = inventories.Split('|', StringSplitOptions.RemoveEmptyEntries);
            Dictionary<string, int> inventoryWithMaterialAmount = new Dictionary<string, int>();

            foreach (string inventory in splittedInventories)
            {
                var inventoryWithAmount = inventory.Split(',');
                int.TryParse(inventoryWithAmount[1], out int amount);
                inventoryWithMaterialAmount.Add(inventoryWithAmount[0], amount);
            }

            return inventoryWithMaterialAmount;
        }

        public static List<Inventory> ParseRecord(string record)
        {
            List<Inventory> inventories = new List<Inventory>();
            if (record.StartsWith("#"))
            {
                return inventories;
            }

            string[] recordItems = record.Split(';', StringSplitOptions.RemoveEmptyEntries);
            
            Dictionary<string, int> inventoriesWithAmount = ParseInventories(recordItems[2]);

            foreach(var inventoryWithAmount in inventoriesWithAmount)
            {
                Inventory inventory = new Inventory();
                inventory.Name = inventoryWithAmount.Key;
                inventory.Materials = new List<Material>();

                Material material = new Material();
                material.Name = recordItems[0];
                material.Id = recordItems[1];
                material.Amount = inventoryWithAmount.Value;

                inventory.Materials.Add(material);
                inventories.Add(inventory);
            }

            return inventories;

        }

        public static List<Inventory> MergeInventories(List<Inventory> inventories, List<Inventory> inventoriesToMerge)
        {
            foreach(var inventoryToMerge in inventoriesToMerge)
            {
                var existingInventory = inventories.FirstOrDefault(i => i.Name.Equals(inventoryToMerge.Name, StringComparison.InvariantCultureIgnoreCase));
                if (existingInventory != null)
                {
                    existingInventory.Materials.AddRange(inventoryToMerge.Materials);
                }
                else
                {
                    inventories.Add(inventoryToMerge);
                }
            }

            return inventories;
        }

        public static List<Inventory> SortInventories(List<Inventory> inventories)
        {
            inventories.ForEach(s => s.Materials = s.Materials.OrderBy(m => m.Id).ToList());
            inventories = inventories.OrderByDescending(r => r.TotalAmount).ThenByDescending(r => r.Name).ToList();

            return inventories;
        }

        public static void PrintInventories(List<Inventory> inventories)
        {
            foreach(var inventory in inventories)
            {
                Console.WriteLine($"{inventory.Name} (total {inventory.TotalAmount})");

                foreach(var material in inventory.Materials)
                {
                    Console.WriteLine($"{material.Id} : {material.Amount}");
                }

                Console.WriteLine();
            }
        }

      
    }
}
