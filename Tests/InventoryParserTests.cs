
using Innergy;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Tests
{
    public class InventoryParserTests
    {
        private List<Inventory> _currentInventories;
        private List<Inventory> _inventoriesToMerge;

        public InventoryParserTests()
        {
            _currentInventories = new List<Inventory>()            {
                new Inventory()
                {
                    Name = "Inv1",
                    Materials = new List<Material>()
                    {
                        new Material()
                        {
                            Name = "Mat4",
                            Id = "Mat4Id",
                            Amount = 4
                        },
                    }
                },
                new Inventory()
                {
                    Name = "Inv3",
                    Materials = new List<Material>()
                    {
                        new Material()
                        {
                            Name = "Mat5",
                            Id = "Mat5Id",
                            Amount = 5
                        },
                    }
                },

            };

            _inventoriesToMerge = new List<Inventory>()
            {
                new Inventory()
                {
                    Name = "Inv1",
                    Materials = new List<Material>()
                    {
                        new Material()
                        {
                            Name = "Mat1",
                            Id = "Mat1Id",
                            Amount = 1
                        },
                        new Material()
                        {
                            Name = "Mat2",
                            Id = "Mat2Id",
                            Amount = 2
                        },
                    }
                },
                new Inventory()
                {
                    Name = "Inv2",
                    Materials = new List<Material>()
                    {
                        new Material()
                        {
                            Name = "Mat3",
                            Id = "Mat3Id",
                            Amount = 3
                        },
                    }
                },

            };
        }

        [Fact]
        public void return_correct_data_for_material_which_is_in_one_inventory()
        {
            string inventories = "WH-A,5";
            string inventoryName = "WH-A";
            int amount = 5;

            var inventoriesWithMaterialAmount = InventoryParser.ParseInventories(inventories);

            inventoriesWithMaterialAmount.Count.ShouldBe(1);

            var inventoryWithMaterial = inventoriesWithMaterialAmount.FirstOrDefault(i => i.Key.Equals(inventoryName, StringComparison.InvariantCultureIgnoreCase));
            inventoryWithMaterial.ShouldNotBeNull();
            inventoryWithMaterial.Key.ShouldBe(inventoryName);
            inventoryWithMaterial.Value.ShouldBe(amount);
        }

        [Fact]
        public void return_correct_data_for_material_which_is_in_two_inventories()
        {
            string inventories = "WH-A,5|WH-B,10";
            string inventoryName1 = "WH-A";
            int amount1 = 5;
            string inventoryName2 = "WH-B";
            int amount2 = 10;

            var inventoriesWithMaterialAmount = InventoryParser.ParseInventories(inventories);

            inventoriesWithMaterialAmount.Count.ShouldBe(2);

            var inventoryWithMaterial1 = inventoriesWithMaterialAmount.FirstOrDefault(i => i.Key.Equals(inventoryName1, StringComparison.InvariantCultureIgnoreCase));
            inventoryWithMaterial1.ShouldNotBeNull();
            inventoryWithMaterial1.Key.ShouldBe(inventoryName1);
            inventoryWithMaterial1.Value.ShouldBe(amount1);

            var inventoryWithMaterial2 = inventoriesWithMaterialAmount.FirstOrDefault(i => i.Key.Equals(inventoryName2, StringComparison.InvariantCultureIgnoreCase));
            inventoryWithMaterial2.ShouldNotBeNull();
            inventoryWithMaterial2.Key.ShouldBe(inventoryName2);
            inventoryWithMaterial2.Value.ShouldBe(amount2);
        }

        [Fact]
        public void return_empty_inventories_for_record_starts_from_invalid_char()
        {
            string record = "# New materials";

            var inventories = InventoryParser.ParseRecord(record);

            inventories.ShouldNotBeNull();
            inventories.Count.ShouldBe(0);
        }

        [Fact]
        public void return_correct_data_for_material_record()
        {
            string record = "Cherry Hardwood Arched Door - PS;COM-100001;WH-A,5|WH-B,10";
            string materialName = "Cherry Hardwood Arched Door - PS";
            string materialId = "COM-100001";
            string inventoryName1 = "WH-A";
            int amount1 = 5;
            string inventoryName2 = "WH-B";
            int amount2 = 10;

            var inventories = InventoryParser.ParseRecord(record);

            inventories.Count.ShouldBe(2);

            var inventory1 = inventories.FirstOrDefault(i => i.Name.Equals(inventoryName1, StringComparison.InvariantCultureIgnoreCase));
            inventory1.ShouldNotBeNull();
            inventory1.Materials.ShouldSatisfyAllConditions(
                () => inventory1.Materials.ShouldNotBeNull(),
                () => inventory1.Materials.First().Id.ShouldBe(materialId),
                () => inventory1.Materials.First().Name.ShouldBe(materialName),
                () => inventory1.Materials.First().Amount.ShouldBe(amount1)
            );

            var inventory2 = inventories.FirstOrDefault(i => i.Name.Equals(inventoryName2, StringComparison.InvariantCultureIgnoreCase));
            inventory2.Name.ShouldBe(inventoryName2);
            inventory2.Materials.ShouldSatisfyAllConditions(
                () => inventory2.Materials.ShouldNotBeNull(),
                () => inventory2.Materials.First().Id.ShouldBe(materialId),
                () => inventory2.Materials.First().Name.ShouldBe(materialName),
                () => inventory2.Materials.First().Amount.ShouldBe(amount2)
            );
        }

        [Fact]
        public void return_correct_empty_list_of_inventories_when_inventories_list_is_empty_and_inventories_to_merge_is_empty()
        {
            List<Inventory> currentInventories = new List<Inventory>();
            List<Inventory> inventoriesToMerge = new List<Inventory>();

            var inventories = InventoryParser.MergeInventories(currentInventories, inventoriesToMerge);

            inventories.ShouldNotBeNull();
            inventories.Count.ShouldBe(0);
        }

        [Fact]
        public void return_correct_list_of_inventories_when_inventories_list_is_empty_and_inventories_to_merge_is_set()
        {
            List<Inventory> currentInventories = new List<Inventory>();

            var inventories = InventoryParser.MergeInventories(currentInventories, _inventoriesToMerge);

            inventories.ShouldNotBeNull();
            inventories.Count.ShouldBe(2);

            var inventory1 = inventories.FirstOrDefault(i => i.Name.Equals("Inv1", StringComparison.InvariantCultureIgnoreCase));
            inventory1.ShouldNotBeNull();
            inventory1.Materials.ShouldSatisfyAllConditions(
                () => inventory1.Materials.ShouldNotBeNull(),
                () => inventory1.Materials.Count.ShouldBe(2),
                () => inventory1.Materials.First().Id.ShouldBe("Mat1Id"),
                () => inventory1.Materials.First().Name.ShouldBe("Mat1"),
                () => inventory1.Materials.First().Amount.ShouldBe(1),
                () => inventory1.Materials.Last().Id.ShouldBe("Mat2Id"),
                () => inventory1.Materials.Last().Name.ShouldBe("Mat2"),
                () => inventory1.Materials.Last().Amount.ShouldBe(2)
            );

            var inventory2 = inventories.FirstOrDefault(i => i.Name.Equals("Inv2", StringComparison.InvariantCultureIgnoreCase));
            inventory2.ShouldNotBeNull();
            inventory2.Materials.ShouldSatisfyAllConditions(
                () => inventory2.Materials.ShouldNotBeNull(),
                () => inventory2.Materials.Count.ShouldBe(1),
                () => inventory2.Materials.First().Id.ShouldBe("Mat3Id"),
                () => inventory2.Materials.First().Name.ShouldBe("Mat3"),
                () => inventory2.Materials.First().Amount.ShouldBe(3)
            );
        }

        [Fact]
        public void return_correct_list_of_inventories_when_inventories_list_is_set_and_inventories_to_merge_is_set()
        {
            var inventories = InventoryParser.MergeInventories(_currentInventories, _inventoriesToMerge);

            inventories.ShouldNotBeNull();
            inventories.Count.ShouldBe(3);

            var inventory1 = inventories.FirstOrDefault(i => i.Name.Equals("Inv1", StringComparison.InvariantCultureIgnoreCase));
            inventory1.ShouldNotBeNull();
            inventory1.Materials.ShouldSatisfyAllConditions(
                () => inventory1.Materials.ShouldNotBeNull(),
                () => inventory1.Materials.Count.ShouldBe(3),
                () => inventory1.Materials.First(m => m.Name.Equals("Mat1", StringComparison.InvariantCultureIgnoreCase)).Id.ShouldBe("Mat1Id"),
                () => inventory1.Materials.First(m => m.Name.Equals("Mat1", StringComparison.InvariantCultureIgnoreCase)).Name.ShouldBe("Mat1"),
                () => inventory1.Materials.First(m => m.Name.Equals("Mat1", StringComparison.InvariantCultureIgnoreCase)).Amount.ShouldBe(1),
                () => inventory1.Materials.First(m => m.Name.Equals("Mat2", StringComparison.InvariantCultureIgnoreCase)).Id.ShouldBe("Mat2Id"),
                () => inventory1.Materials.First(m => m.Name.Equals("Mat2", StringComparison.InvariantCultureIgnoreCase)).Name.ShouldBe("Mat2"),
                () => inventory1.Materials.First(m => m.Name.Equals("Mat2", StringComparison.InvariantCultureIgnoreCase)).Amount.ShouldBe(2),
                () => inventory1.Materials.First(m => m.Name.Equals("Mat4", StringComparison.InvariantCultureIgnoreCase)).Id.ShouldBe("Mat4Id"),
                () => inventory1.Materials.First(m => m.Name.Equals("Mat4", StringComparison.InvariantCultureIgnoreCase)).Name.ShouldBe("Mat4"),
                () => inventory1.Materials.First(m => m.Name.Equals("Mat4", StringComparison.InvariantCultureIgnoreCase)).Amount.ShouldBe(4)
            );                                                          

            var inventory2 = inventories.FirstOrDefault(i => i.Name.Equals("Inv2", StringComparison.InvariantCultureIgnoreCase));
            inventory2.ShouldNotBeNull();
            inventory2.Materials.ShouldSatisfyAllConditions(
                () => inventory2.Materials.ShouldNotBeNull(),
                () => inventory2.Materials.Count.ShouldBe(1),
                () => inventory2.Materials.First().Id.ShouldBe("Mat3Id"),
                () => inventory2.Materials.First().Name.ShouldBe("Mat3"),
                () => inventory2.Materials.First().Amount.ShouldBe(3)
            );

            var inventory3 = inventories.FirstOrDefault(i => i.Name.Equals("Inv3", StringComparison.InvariantCultureIgnoreCase));
            inventory3.ShouldNotBeNull();
            inventory3.Materials.ShouldSatisfyAllConditions(
                () => inventory3.Materials.ShouldNotBeNull(),
                () => inventory3.Materials.Count.ShouldBe(1),
                () => inventory3.Materials.First().Id.ShouldBe("Mat5Id"),
                () => inventory3.Materials.First().Name.ShouldBe("Mat5"),
                () => inventory3.Materials.First().Amount.ShouldBe(5)
            );
        }

    }
}
